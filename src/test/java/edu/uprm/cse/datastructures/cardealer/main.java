package edu.uprm.cse.datastructures.cardealer;

import java.util.Comparator;
import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.main.IntComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA.MapEntry;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedListImp;



public class main {
	

	public static class IntComparator implements Comparator<Integer> {

		public int compare(Integer arg0, Integer arg1) {
			
			return arg0 < arg1?-1: arg0 > arg1 ? +1:0;
		}

	}

	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		
		HashTableOA<String,Integer> tester = new HashTableOA(20, new IntComparator(), null);
		
		SortedListImp<Integer> memer = new SortedListImp<Integer>(new IntComparator());
		
		memer.add(10);
		memer.add(10);
		memer.add(4);
		memer.add(4);
		memer.add(5);
		memer.add(4);
		
//		System.out.println(memer);
		
//		for(int i : memer) {
//			System.out.println(i);
//		}
		
		
		
		
		tester.put("Meme", 10);
		tester.put("Test", 5);
		tester.put("Zest", 4);
		
//		System.out.println(tester.get("Meme"));
//		tester.remove("Meme");
//		System.out.println(tester.get("Meme"));
	
		
		SortedList<Integer> a = tester.getValues();
		
		for(int i=0;i<a.size();i++) {
			System.out.println(a.get(i));
		}
		
		
		System.out.println("===MEME ZONE===");
		Integer t = tester.get("Meme");
		System.out.println(t);
		System.out.println(tester.get("Test"));
		System.out.println(tester.get("Zest"));
		System.out.println(tester.contains("Test"));
		System.out.println(tester.contains("Trt"));
		
	//	tester.remove("Meme");
		
		System.out.println(tester.get("Meme")+"\n");
		
//	 SortedList<String> u = tester.getKeys();
//	
//	System.out.println("===KEYS ZONE===");
//	for(int i=0;i<u.size();i++) {
//		
//		System.out.println(u.get(i));
//		
//	}
	
	SortedList<Integer> r = 	tester.getValues();
	
	
		System.out.println("===Values ZONE===");
//		for(int i=0;i<r.size();i++) {
//			
//			System.out.println(r.get(i));
//			
//		}
		
		Iterator<Integer> rit = r.iterator();
		while(rit.hasNext()) {
			System.out.println(rit.next());
		}
	
}
	
	}


