package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.List;

public class TwoThreeTree<K, V> extends BTree {
	
	private class TwoNode extends TreeNode{

		TwoNode(MapEntry entryA, MapEntry entryB, Comparator comparator) {
			super(entryA, entryB, comparator);
		}
		
		public void exchange() {
			Object nodeTemp = this.entries.get(0);
			this.entries.remove(0);
			this.entries.add(nodeTemp);
		}
		
		
		
	}
	
	
	
	
	int currentSize;
	int deleted;
	Comparator keyComp;
	
	private TwoNode root;
	
	
	
	public TwoThreeTree(TwoNode root,Comparator keyComparator) {
		super(keyComparator);
		this.root = root;
		this.keyComp = keyComparator;
	}

//	public TwoThreeTree(Comparator keyComparator) {
//		this(new TreeNode(null, null, keyComparator),keyComparator);
//	}

	
	
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
	return this.currentSize == 0;
	}

	@Override
	public Object get(Object key) {
		if(this.contains(key)) {
			
		}
		return null;
	}


	@Override
	public Object put(Object key, Object value) {
		if(value==null) {
			throw new IllegalArgumentException("Null is not a valid value.");
		}
		
		if(root == null) {
			this.root = new TwoNode(new MapEntry(key, value, this.keyComparator), null, keyComp);
			this.currentSize++;
			return this.root;
		}
		putAux(new MapEntry(key,value,this.keyComparator),root);
		
		
		
		
		
		
		
		return null;
	}

	private void putAux(MapEntry mapEntry, TreeNode root) {
		if(root.entries.size()<2) {
			root.entries.add(mapEntry);
			return;
		}
		else {
			split(root);
			return;
		}
		
		
	}

	@Override
	public Object remove(Object key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean contains(Object key) {
		for(int i=0;i<this.size();i++) {
			if(this.root.entries.get(i) == key) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List getKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getValues() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	boolean isLeaf(TreeNode treeNode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	void split(TreeNode treeNode) {
	 if(this.root==treeNode) {
		
	 }
		
	}

}
