package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.LongComparator;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {

	private static HashTableOA<Long,Car> carList = new HashTableOA<>(new LongComparator(),new CarComparator());
	
	
	private CarTable() {
		CarTable.carList = new HashTableOA<>(new LongComparator(),new CarComparator());
	}
	
	public static HashTableOA<Long,Car> getInstance(){
		return carList;
	}

	
	public static void resetCars() {
		CarTable.carList = new HashTableOA<>(new LongComparator(),new CarComparator());
	}

	
}
