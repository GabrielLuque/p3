package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.List;


public class HashTableOA<K, V> implements Map<K, V> {
	
	public static class MapEntry<K,V> {
	
		private V value;
		private K key;
		
		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}
		
		public V getValue() {
			return value;
		}
		
		public K getKey() {
			return key;
		}
		
		
		public void setValue(V value) {
			this.value = value;
		}
		
		public void setKey(K key) {
			this.key = key;
		}
	
		
		
		
	}
	
	
	
	private static final int DEF_SLOT = 11;
	
	private int currentSize;
	private MapEntry<K,V>[] slots;

	private Comparator<K> keyComp;
	private Comparator<V> valComp;
	
	
	public HashTableOA(int numBuckets, Comparator<K> keyComp, Comparator <V> valComp) {
		this.currentSize  = 0;
		this.keyComp = keyComp;
		this.valComp = valComp;
		this.slots = (MapEntry<K, V>[]) new MapEntry[numBuckets];
		
	}
	
	public HashTableOA( Comparator<K> keyComp, Comparator <V> valComp) {
		this(DEF_SLOT, keyComp,valComp);
	}
	

	
	
	private int hashFunction(K key) {
		return ((~(key.hashCode()*42))>>>2) % this.slots.length;
	
	
	}
	
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		//Fixed		
		if(this.contains(key)) {
		return this.slots[this.hashFunction(key)].getValue();
		}
		else {
		return null;
		}
	}

	@Override
	public V put(K key, V value) {	
		//Fixed
		if(this.slots[this.hashFunction(key)]!=null) {
			
			for(int i=this.hashFunction(key)+1;i<this.size();i++) {
				
				if(this.slots[i]==null) {
					this.currentSize++;
					this.slots[i] = new MapEntry(key,value);
					return ((MapEntry<K, V>)this.slots[i]).getValue();
					
				}	
			}
		}
		
		this.slots[this.hashFunction(key)] = new MapEntry(key,value);		
		this.currentSize++;
		return ((MapEntry<K, V>)this.slots[this.hashFunction(key)]).getValue();
	}

	@Override
	public V remove(K key) {
		//Fixed
		if(!this.contains(key)) {
			return null;
		}
		else {
		V removed = this.get(key);
		
		this.slots[this.hashFunction(key)] = null;
		this.currentSize--;
		return removed;
		}
		
	}

	@Override
	public boolean contains(K key) {
		// Fixed
		return  this.slots[this.hashFunction(key)] != null;
	}

	@Override
	public List<K> getKeys() {
	// SortedListImp<K> result = new SortedListImp<K>(this.comparator);
	 List<K> result = (List<K>) new CircularSortedDoublyLinkedList<K>(this.keyComp);
		for(int i=0;i<this.slots.length;i++) {
			if(this.slots[i]!=null) {
			result.add(this.slots[i].getKey());
			}
			
		}return result;
		
		
		
		
	}

	@Override
	public List<V> getValues() {
		// SortedListImp<V> result = new SortedListImp<V>(this.comparator);
	List<V> result = (List<V>) new CircularSortedDoublyLinkedList<V>(this.valComp);
		for(int i=0;i<this.slots.length;i++) {
			if(this.slots[i]!=null) {
			result.add( this.slots[i].getValue());
		}
		}
	
		return result;
	}

}